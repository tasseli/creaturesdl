#pragma once

#include <fstream>
#include <iostream>
#include <vector>
#include <stdlib.h>     // srand()
#include <time.h>       // seeding rand()
#include "SDL/SDL.h"                                // show easy graphics

#include "Critter.h"       // my creatures <3
#include "graphics.h"  // graphic routines copied and edited

bool loopProgram();
