# README #

Small life sim, picturing creatures with a single pixel. The creatures have a genome, display a fenotype, and move around, multiply bisexually when able, and show their states with temporary colors that adjust to their more natural color in stages.
